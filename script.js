const month=[31,28,31,30,31,30,31,31,30,31,30,31];
function calculateage(){
    console.log("button  cliked");
    const dob=document.getElementById("date_of_birth").value;
    console.log(dob);
    const gd= document.getElementById("given_date").value;

    let startd= new Date(dob);
    let endd = new Date(gd);

    let year1=startd.getFullYear();
    let month1=startd.getMonth();
    let day1=startd.getDay();

    let year2=endd.getFullYear();
    let month2=endd.getMonth();
    let day2=endd.getDay();

    leapTest(year2);
    let diffyear= year2-year1;
    if(
        year1>year2 || 
        ( month1>month2 && year1===year2) ||
        (day1>day2 && month1===month2 && year1===year2) 
    ){
        alert("must be born")
    }

    let diffmonth ;
    if(month2>=month1){
        diffmonth= month2-month1;
    }
    else{
        diffyear--;
        diffmonth=12-month2-month1;
    }

    let diffday;
    if(day2>=day1){
        diffday=day2-day1;
    }else{
        diffmonth--;
        let days=month[month2-2];
        diffday= days+day2-day1;
        if(diffday<0){
            diffmonth=11;
            diffyear--;
        }

    }

    console.log(`${diffyear}year ${diffmonth}month ${diffday}days`);
    showResults(diffday,diffmonth,diffyear);


}
const showResults=(totaldays,totalmonths,totalyears)=>{
    document.getElementById("years").textContent=`${totalyears} years`;
    document.getElementById("months").textContent=`${totalmonths} months`;
    document.getElementById("days").textContent=`${totaldays} days`;
    document.getElementById("output_box").style.display="flex";
    document.getElementById("msg").style.display="block";
}

function leapTest(year){
if((year % 4==4) && (year%100!=0)  || (year%400==0)){
    month[1]=29;
}
else{
    month[1]=28;
}
}